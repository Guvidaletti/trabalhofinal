package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javafx.collections.ObservableList;

public class GerenciadorPaises {

    private Map<String, Pais> paises; // codigo do pais e pais

    public GerenciadorPaises() {
        paises = new HashMap<>();
    }

    public void adicionar(String key, Pais value) {
        paises.put(key, value);
    }

    public void adicionarAeroporto(String key, Aeroporto aero) {
        Pais p = paises.get(key);
        p.adicionarAeroporto(aero);
    }

    public Map<String, Pais> getPaises() {
        return paises;
    }

    public Pais getPais(String key) {
        return paises.get(key);
    }

    public ArrayList<Pais> toArray() {
        ArrayList<Pais> temp_paises = new ArrayList<Pais>();
        for(String key : paises.keySet()) {
            temp_paises.add(paises.get(key));
        }
        return temp_paises;
    }

    public ArrayList<Pais> toArrayOrdenado() {
        ArrayList<Pais> temp_paises = new ArrayList<Pais>();
        for(String key : paises.keySet()) {
            temp_paises.add(paises.get(key));
        }
        ordenarNome(temp_paises);
        return temp_paises;
    }

    public static void ordenarNome(ArrayList<Pais> listaPaises) {
        //Collections.sort(listaPaises);
        listaPaises.sort( (Pais p1, Pais p2)
                -> p1.getNome().compareTo(p2.getNome()));
    };

    public void carregaPaises() throws IOException {
        //System.out.println("### Carregando Aeroportos ###\n\n");
        Path path2 = Paths.get("countries.dat");
        try (Scanner sc = new Scanner(Files.newBufferedReader(path2, Charset.forName("utf8")))) {
            sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
            String header = sc.nextLine(); // pula cabecalho
            String codigo, nome;
            while (sc.hasNext()) {
                codigo = sc.next();
                nome = sc.next().replaceAll("(\r)", "");
                Pais p = new Pais(codigo, nome);
                adicionar(codigo, p);

            }
        }
        catch (IOException x) {
            System.err.format("Erro de E/S: %s%n", x);
        }
    }

    public void carregaAeroportos(GerenciadorAeroportos gerAero) {
        //System.out.println("### Carregando Aeroportos nos Paises ###");
        for(int i = 0; i < gerAero.toArray().size(); i++) {
            Aeroporto a = gerAero.toArray().get(i);
            String cod_pais = a.getCodigoPais().replaceAll("(\r)", "");
            Pais p = paises.get(cod_pais);
            //System.out.println(a + " - no país: " + p);
            p.adicionarAeroporto(a);
        }
    }
}
