package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;

public class GerenciadorRotas {

    private ArrayList<Rota> rotas;


    public GerenciadorRotas() {
        this.rotas = new ArrayList<>();
    }

    public void ordenarCias() {Collections.sort(rotas);}
    public void ordenarNomesCias() {
        rotas.sort((Rota r1, Rota r2) -> r1.getCia().getNome().compareTo(r2.getCia().getNome()));
    }

    public void ordenarNomesAeroportos() {
        rotas.sort( (Rota r1, Rota r2) -> r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome()));
    }

    public void ordenarNomesAeroportosCias() {
        rotas.sort( (Rota r1, Rota r2) -> {
           int result = r1.getOrigem().getNome().compareTo(
                   r2.getOrigem().getNome());
           if(result != 0)
               return result;
           return r1.getCia().getNome().compareTo(
                   r2.getCia().getNome());
        });
    }

    public void adicionar(Rota r) {
        rotas.add(r);
    }

    public ArrayList<Rota> toArray() {
        return rotas;
    }

    public ArrayList<Rota> listarTodas() {
        return new ArrayList<>(rotas);
    }

    public ArrayList<Rota> buscarOrigem(String codigoAero) {
        ArrayList<Rota> pesquisa = new ArrayList<>();
        for(Rota r: listarTodas()){
            if(r.getOrigem().getCodigo().equalsIgnoreCase(codigoAero))
                pesquisa.add(r);
        }
        return pesquisa;
    }

    public void carregaRotas(GerenciadorAeronaves gerAvioes , GerenciadorAeroportos gerAero, GerenciadorCias gerCias) throws IOException {
        Path path = Paths.get("routes.dat");
        try(Scanner sc = new Scanner(Files.newBufferedReader(path,Charset.forName("utf8")))){
            sc.useDelimiter("[;\n]");
            String header = sc.nextLine();//pula linha
            String cia,de,para,codeshare,stops,equipment;
            while(sc.hasNext()){
                //LEITURA
                cia = sc.next();
                de=sc.next();
                para=sc.next();
                codeshare=sc.next(); //pula
                stops=sc.next(); // pula
                equipment=sc.next();
                //OPERACOES
                String[] splitEquipment = equipment.split("\\s"); // ou \\s+
                String codAviao = splitEquipment[0];
                Aeronave equipamento = gerAvioes.buscarCodigo(codAviao);
                //if(equipamento==null) System.out.println(codAviao);
                Aeroporto origem = gerAero.buscarCodigo(de);
                Aeroporto destino = gerAero.buscarCodigo(para);
                CiaAerea companhia = gerCias.buscarCodigo(cia);
                //ADICIONAR ROTA
                if(equipamento != null)
                {
                    Rota nova = new Rota(companhia,origem,destino,equipamento);
                    adicionar(nova);
                }
            }
        }
    }
}