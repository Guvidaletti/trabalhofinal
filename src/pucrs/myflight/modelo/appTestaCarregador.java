package pucrs.myflight.modelo;

import javafx.collections.FXCollections;

import java.io.IOException;

public class appTestaCarregador {
    public static void main(String[] args) throws IOException {
        GerenciadorAeronaves gerAvioes = new GerenciadorAeronaves();
        GerenciadorAeroportos gerAeroportos = new GerenciadorAeroportos();
        GerenciadorPaises gerPaises = new GerenciadorPaises();
        GerenciadorCias gerCias = new GerenciadorCias();
        GerenciadorRotas gerRotas = new GerenciadorRotas();

        gerAvioes.carregarAeronaves();
        gerAeroportos.carregaAeroportos();
        gerPaises.carregaPaises();
        gerCias.carregaCias();
        gerRotas.carregaRotas(gerAvioes,gerAeroportos,gerCias);

        System.out.println(FXCollections.observableArrayList(gerCias.toArray()));
        

    }
}
