package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class GerenciadorAeroportos {

    private Map<String, Aeroporto> aeroportos;

    public GerenciadorAeroportos() {
        aeroportos = new TreeMap<String, Aeroporto>();
    }

    public void adicionar(Aeroporto aero) {
        aeroportos.put(aero.getCodigo(), aero);
    }

    //	public void ordenarNome() {
//		//Collections.sort(aeroportos);
//		aeroportos.sort( (Aeroporto a1, Aeroporto a2)
//				-> a1.getNome().compareTo(a2.getNome()));
//	};
//
    public static ArrayList<Aeroporto> ordenarCodigo(ArrayList<Aeroporto> aeroportos_s) {
        aeroportos_s.sort( (Aeroporto a1, Aeroporto a2) -> a1.getCodigo().compareTo(a2.getCodigo()));
        return aeroportos_s;
    }
//
//	public void ordenarPais() {
//		aeroportos.sort( (Aeroporto a1, Aeroporto a2)
//				-> a1.getCodigoPais().compareTo(a2.getCodigoPais()));
//	};

    public ArrayList<Aeroporto> toArray() {
        ArrayList<Aeroporto> temp_aero = new ArrayList<Aeroporto>();
        for(String key : aeroportos.keySet()) {
            temp_aero.add(aeroportos.get(key));
        }
        return temp_aero;
    }

    public Aeroporto buscarCodigo(String codigo) {
        return aeroportos.get(codigo);
    }

    public static void print (ArrayList<Aeroporto> aeroportos) {
        if(aeroportos == null) return;
        for(Aeroporto a : aeroportos) {
            if(a != null) {
                System.out.println(a);
            }

        }
    }

    public void carregaAeroportos() throws IOException {
        //System.out.println("### Carregando Aeroportos ###\n\n");
        Path path2 = Paths.get("airports.dat");
        try (Scanner sc = new Scanner(Files.newBufferedReader(path2, Charset.forName("utf8")))) {
            sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
            String header = sc.nextLine(); // pula cabeçalho
            String codigo, nome, pais;
            String slat, slon;
            while (sc.hasNext()) {
                codigo = sc.next();
                slat = sc.next();
                slon = sc.next();
                nome = sc.next();
                pais = sc.next().replaceAll("(\r)", ""); // incrementado devido a novo arquivo, alterado construtor
                //System.out.println(pais);
                Geo geo = new Geo(Double.parseDouble(slat), Double.parseDouble(slon));
                Aeroporto nova = new Aeroporto(codigo, nome, geo, pais);
                adicionar(nova);
                //System.out.format("%s - %s - %s - %s", codigo, nome, geo, pais );
            }
        }
        catch (IOException x) {
            System.err.format("Erro de E/S: %s%n", x);
        }
//		}
        //System.out.println("### Encerrado Carregamento Aeroportos ###\n\n");
    }
}
