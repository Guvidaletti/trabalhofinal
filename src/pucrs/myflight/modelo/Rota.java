package pucrs.myflight.modelo;

import java.time.Duration;

public class Rota implements Comparable<Rota> {
	private CiaAerea cia;
	private Aeroporto origem;
	private Aeroporto destino;
	private Aeronave aeronave;
	
	public Rota(CiaAerea cia, Aeroporto origem, Aeroporto destino, Aeronave aeronave) {
		this.cia = cia;
		this.origem = origem;
		this.destino = destino;
		this.aeronave = aeronave;		
	}	
	
	public CiaAerea getCia() {
		return cia;
	}
	
	public Aeroporto getDestino() {
		return destino;
	}
	
	public Aeroporto getOrigem() {
		return origem;
	}
	
	public Aeronave getAeronave() {
		return aeronave;
	}

    public Duration getDuration()
    {
        double tempo=0;
        double dist=0;
        dist += getOrigem().getLocal().distancia(getDestino().getLocal());
        tempo += (dist / 805.0)*60.0;
        //tempo += (getTotalRotas()*30);
        return Duration.ofMinutes(Math.round(tempo));
    }


    @Override
    public String toString() {
        return cia.getCodigo() + " - " + origem.getCodigo() + " -> " + destino.getCodigo()+
		" ("  + aeronave.getCodigo() + ")";
    }

	@Override
	public int compareTo(Rota rota) {
		return this.cia.getNome().compareTo(
				rota.cia.getNome());
	}
}
