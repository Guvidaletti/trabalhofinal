package pucrs.myflight.gui;

import java.awt.*;
import java.awt.event.*;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;

import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javax.swing.SwingUtilities;

import javafx.beans.InvalidationListener;
import javafx.collections.ListChangeListener;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoPosition;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import pucrs.myflight.modelo.*;

public class JanelaFX extends Application {

    final SwingNode mapkit = new SwingNode();

    private GerenciadorCias gerCias;
    private GerenciadorAeroportos gerAero;
    private GerenciadorRotas gerRotas;
    private GerenciadorAeronaves gerAvioes;
    private GerenciadorPaises gerPaises;

    private GerenciadorMapa gerenciador;

    private EventosMouse mouse;

    private ObservableList<CiaAerea> comboCiasData;
    private ComboBox<CiaAerea> comboCia;
    private ComboBox<Aeroporto> origem3, destino3;
    private ComboBox<Pais> comboPaises;
    private TextField asHoras;
    private CiaAerea ciaEscolhida;
    private Pais paisEscolhido;

    @Override
    public void start(Stage primaryStage) throws Exception {

        setup();
        //INICIALIZAÇÃO DOS ARTEFATOS PARA UTILIZAÇÃO DO PROGRAMA
        asHoras = new TextField();
        asHoras.setEditable(true);
        asHoras.setPromptText("Horas");
        asHoras.setPrefWidth(120);
        Button btnZerar = new Button();
        btnZerar.setPrefWidth(120);
        btnZerar.setVisible(true);
        btnZerar.setText("Limpar Mapa");
        comboCia = new ComboBox<>();
        comboCia.getItems().addAll(gerCias.listarTodas());
        comboPaises = new ComboBox<>();
        comboPaises.getItems().add(new Pais("00", "Todos os Países"));
        comboPaises.getItems().addAll(gerPaises.toArrayOrdenado());
        origem3 = new ComboBox<>();
        destino3 = new ComboBox<>();
        origem3.getItems().addAll(gerAero.toArray());
        destino3.getItems().addAll(gerAero.toArray());


        GeoPosition poa = new GeoPosition(-30.05, -51.18);
        gerenciador = new GerenciadorMapa(poa, GerenciadorMapa.FonteImagens.VirtualEarth);
        mouse = new EventosMouse();
        gerenciador.getMapKit().getMainMap().addMouseListener(mouse);
        gerenciador.getMapKit().getMainMap().addMouseMotionListener(mouse);

        createSwingContent(mapkit);

        BorderPane pane = new BorderPane();
        GridPane leftPane = new GridPane();

        origem3.setEditable(true);
        destino3.setEditable(true);
        leftPane.setAlignment(Pos.CENTER);
        leftPane.setHgap(10);
        leftPane.setVgap(10);
        leftPane.setPadding(new Insets(10, 10, 10, 10));

        comboCia.setMaxWidth(120);
        comboCia.setPromptText("Cias");
        comboCia.setVisible(true);
        comboPaises.setVisible(true);
        comboPaises.setMaxWidth(120);
        comboPaises.setPromptText("Países");
        origem3.setMaxWidth(120);
        origem3.setPromptText("Origem");
        origem3.setVisible(true);
        destino3.setMaxWidth(120);
        destino3.setPromptText("Destino");
        destino3.setVisible(true);


        Button btnConsulta1 = new Button("Consulta 1");
        Button btnConsulta2 = new Button("Consulta 2");
        Button btnConsulta3 = new Button("Consulta 3");
        Button btnConsulta4 = new Button("Consulta 4");
        btnConsulta1.setMaxWidth(120);
        btnConsulta1.setPrefWidth(120);
        btnConsulta2.setMaxWidth(120);
        btnConsulta2.setPrefWidth(120);
        btnConsulta3.setMaxWidth(120);
        btnConsulta3.setPrefWidth(120);
        btnConsulta4.setMaxWidth(120);
        btnConsulta4.setPrefWidth(120);
        leftPane.add(comboCia, 0, 0);
        leftPane.add(btnConsulta1, 1, 0);
        leftPane.add(comboPaises, 0, 1);
        leftPane.add(btnConsulta2, 1, 1);

        leftPane.add(origem3, 3, 0);
        leftPane.add(destino3, 4, 0);
        leftPane.add(btnConsulta3, 5, 0);

        leftPane.add(asHoras, 3, 1);
        leftPane.add(btnConsulta4, 4, 1);

        leftPane.add(btnZerar, 5, 1);


        btnZerar.setOnAction(e -> {
            zeraMapa();
        });

        btnConsulta1.setOnAction(e -> {
            ciaEscolhida = comboCia.getValue();
            if (ciaEscolhida != null) consulta1(ciaEscolhida);
        });

        btnConsulta2.setOnAction(e -> {
            paisEscolhido = comboPaises.getValue();
            if (paisEscolhido != null) consulta2(paisEscolhido);
        });

        btnConsulta3.setOnAction(event -> {
            Aeroporto orig = null;
            Aeroporto dest = null;
            try {
                orig = origem3.getValue();
                dest = destino3.getValue();
                consulta3(orig, dest);
            } catch (ClassCastException e) {
                try {
                    String o = origem3.getEditor().getText();
                    String d = destino3.getEditor().getText();

                    String[] oSplit = o.split("\\s");
                    String[] dSplit = d.split("\\s");

                    orig = gerAero.buscarCodigo(oSplit[0].toUpperCase());
                    dest = gerAero.buscarCodigo(dSplit[0].toUpperCase());
                    consulta3(orig, dest);
                } catch (NullPointerException d) {
                    //AQUI ALERTA
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Erro");
                    alert.setHeaderText(null);
                    alert.setContentText("Não Foi Possível Executar a busca!");
                    alert.showAndWait();
                }
            } catch (NullPointerException f) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Erro");
                alert.setHeaderText(null);
                alert.setContentText("Digite um valor!");
                alert.showAndWait();
            }

        });

        btnConsulta4.setOnAction(event -> {
            String horasRecebidas = asHoras.getText();
            int hr = -1;
            try {
                hr = Integer.parseInt(horasRecebidas);
            } catch (NumberFormatException e) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Erro");
                alert.setHeaderText(null);
                alert.setContentText("Digite o número de horas!");
                alert.showAndWait();
                return;
            }
            int min = hr * 60;
            consulta4(min);
//		    consulta44(min);
        });


        origem3.setOnKeyPressed(e ->
                {
                    if (e.getCode().getName().equalsIgnoreCase("ENTER")) {
                        String digitado = origem3.getEditor().getText();
                        //System.out.println(digitado);
                        Aeroporto aux = gerAero.buscarCodigo(digitado.toUpperCase());
                        origem3.setValue(aux);
                    }
                }
        );

        destino3.setOnKeyPressed(e ->
                {
                    if (e.getCode().getName().equalsIgnoreCase("ENTER")) {
                        String digitado = destino3.getEditor().getText();
                        //System.out.println(digitado);
                        Aeroporto aux = gerAero.buscarCodigo(digitado.toUpperCase());
                        destino3.setValue(aux);
                    }
                }
        );
        pane.setCenter(mapkit);
        pane.setTop(leftPane);

        Scene scene = new Scene(pane, 650, 650);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Mapas com JavaFX");
        primaryStage.show();

    }

    private void setup() {
        // Carrega os dados aqui
        gerPaises = new GerenciadorPaises();
        try {
            gerPaises.carregaPaises();
        } catch (IOException e) {
            System.err.println("Nao foi possivel carregar banco de dados de Companhias");
        }

        gerCias = new GerenciadorCias();
        try {
            gerCias.carregaCias();
        } catch (IOException e) {
            System.err.println("Nao foi possivel carregar banco de dados de Companhias");
        }

        gerAero = new GerenciadorAeroportos();
        try {
            gerAero.carregaAeroportos();
        } catch (IOException e) {
            System.err.println("Nao foi possivel carregar banco de dados de Aeroportos");
        }

        gerAvioes = new GerenciadorAeronaves();
        try {
            gerAvioes.carregarAeronaves();
        } catch (IOException e) {
            System.err.println("Nao foi possivel carregar banco de dados de Aeroportos");
        }

        gerRotas = new GerenciadorRotas();
        try {
            gerRotas.carregaRotas(gerAvioes, gerAero, gerCias);
        } catch (IOException e) {
            System.err.println("Nao foi possivel carregar banco de dados de Aeroportos");
        }
        gerPaises.carregaAeroportos(gerAero);
    }

    //Metodo que limpa o mapa
    private void zeraMapa() {
        zeraBotoes();
        gerenciador.clear();
        List<MyWaypoint> lstPoints = new ArrayList<>();
        Aeroporto a = new Aeroporto("teste", "teste", new Geo(0, 0));
        Color cor = new Color(0, 0, 0, 0);
        lstPoints.add(new MyWaypoint(cor, a.getNome() + " (" + a.getCodigo() + ")", a.getLocal(), 0));
        gerenciador.setPontos(lstPoints);
        gerenciador.getMapKit().repaint(); // Redesenha o mapa
        //jeito de redesenhar o mapa
    }

    //Metodo que limpa o que esta selecionado nos botoes e combobox
    private void zeraBotoes() {
        comboCia.setValue(null);
        comboPaises.setValue(null);
        origem3.setValue(null);
        destino3.setValue(null);
        asHoras.setText(null);

    }


    /**
     * CONSULTA 1
     * Desenhar todos os aeroportos onde uma determinada companhia aérea opera.
     * Mostre também as rotas envolvidas.
     */
    private void consulta1(CiaAerea c) {

        CiaAerea cia = ciaEscolhida;
        //System.out.println(ciaEscolhida.getNome());

        //PEGA AEROPORTOS EM QUE CIA ATUA-------USAR HASHSET PARA NAO TER ENTRADAS REPETIDAS
        gerenciador.clear();
        Set<Aeroporto> aeroportosDaCia = new HashSet<Aeroporto>();
        for (Rota rota : gerRotas.toArray()) {
            if (rota.getCia().getCodigo().equals(cia.getCodigo())) {
                Aeroporto origem = rota.getOrigem();
                Aeroporto destino = rota.getDestino();

                double distancia = Geo.distancia(origem.getLocal(), destino.getLocal());
                String aviao = rota.getAeronave().toString();

                aeroportosDaCia.add(origem);
                aeroportosDaCia.add(destino);

                //FAZ A LINHA DAS ROTAS ENTRE ORIGEM E DESTINO
                Tracado tr2 = new Tracado();
                tr2.setWidth(2);
                tr2.setCor(Color.BLACK);
                tr2.addPonto(origem.getLocal());
                tr2.addPonto(destino.getLocal());
                tr2.setLabel("Dist:" + distancia + "Aeronave:" + aviao);
                gerenciador.addTracado(tr2);

            }
        }

        //PINTA OS AEROPORTOS DA CIA
        List<MyWaypoint> lstPoints = new ArrayList<>();

        for (Aeroporto a : aeroportosDaCia) {
            System.out.println(a.getNome());
            Tracado tr = new Tracado();
            tr.addPonto(a.getLocal());
            lstPoints.add(new MyWaypoint(Color.RED, a.getNome() + " (" + a.getCodigo() + ")", a.getLocal(), 6));
        }

        gerenciador.setPontos(lstPoints);
        gerenciador.getMapKit().repaint(); // Redesenha o mapa
        zeraBotoes();
    }


    /**
     * CONSULTA DOIS
     * Exibir uma estimativa de volume de tráfego de todos os
     * aeroportos ou de um país específico. Deve-se explorar o
     * tamanho e cor do ponto correspondente, para destacar
     * aeroportos com maior ou menor tráfego.
     */
    private void consulta2(Pais recebido) {
        System.out.println("Consulta2, com: " + recebido.getNome());
        gerenciador.clear();//zerar
        for (Aeroporto a : gerAero.toArray()) a.fluxo = 0;//zerar o fluxo das consultas anteriores

        ArrayList<Aeroporto> Aeros = new ArrayList<>();
        if (recebido.getNome().equalsIgnoreCase("Todos os Países") ||
                recebido.getCodigo().equalsIgnoreCase("00")) { //SE FOR TODOS OS PAÍSES
            //FAZER A OPERAÇAO COM TODOS OS AEROPORTOS DA LISTA
            Aeros = gerAero.toArray();

            for (Rota rota : gerRotas.listarTodas()) {
                rota.getOrigem().fluxo += 1;
                rota.getDestino().fluxo += 1;
            }
        } else { //Se for só um país, recebe os aeroportos do país

            Aeros = recebido.getAeroportos();
            for (Rota rota : gerRotas.listarTodas()) {
                //só adiciona ao fluxo se tiver no país.
                if (rota.getOrigem().getPais().equalsIgnoreCase(recebido.getCodigo())) rota.getOrigem().fluxo += 1;
                if (rota.getDestino().getPais().equalsIgnoreCase(recebido.getCodigo())) rota.getDestino().fluxo += 1;
            }
        }
        Aeros.sort(Comparator.comparing(a -> a.fluxo));
        Collections.reverse(Aeros);
        List<MyWaypoint> pontos = new ArrayList<>();
        Color cor = null;
        int tamanho = 0;
        for (Aeroporto a : Aeros) {
            if (a.fluxo <= 40) {
                cor = new Color(0, 0, 0);
                tamanho = 2;
            } else if (a.fluxo <= 100) {
                cor = new Color(1, 9, 50);
                tamanho = 3;
            } else if (a.fluxo <= 200) {
                cor = new Color(36, 11, 64);
                tamanho = 6;
            } else if (a.fluxo <= 300) {
                cor = new Color(51, 9, 73);
                tamanho = 8;
            } else if (a.fluxo <= 500) {
                cor = new Color(89, 9, 56);
                tamanho = 13;
            } else if (a.fluxo <= 700) {
                cor = new Color(144, 10, 41);
                tamanho = 18;
            } else if (a.fluxo <= 900) {
                cor = new Color(192, 11, 41);
                tamanho = 23;
            } else {
                cor = new Color(255, 6, 24);
                tamanho = 28;
            }


            Tracado tr = new Tracado();
            tr.setLabel(a.getNome() + "(" + a.getCodigo() + ")");
            tr.addPonto(a.getLocal());
            pontos.add(new MyWaypoint(cor, a.getNome() + " (" + a.getCodigo() + ")", a.getLocal(), tamanho));

        }
        gerenciador.setPontos(pontos);
        gerenciador.getMapKit().repaint();
        zeraBotoes();

    }

    /**
     * CONSULTA 3
     * Selecionar um aeroporto de origem e um de destino e
     * mostrar todas as rotas possíveis entre os dois,
     * considerando que haverá no máximo 2 conexões entre eles.
     */
    private void consulta3(Aeroporto orig, Aeroporto dest) {
        gerenciador.clear();
        ArrayList<Rota> rotas = new ArrayList<>();

        //ArrayList<Rota> originada = new ArrayList<>();
        //ArrayList<Aeroporto> meio = new ArrayList<>();
        //ArrayList<Rota> destinos = new ArrayList<>();
        for (Rota r1 : gerRotas.listarTodas()) {
            if (r1.getOrigem() == orig) { //se a saída for a orig
                Aeroporto aux = r1.getDestino(); //aeropoto aux
                if (aux == dest) {
                    rotas.add(r1);
                }
                for (Rota r2 : gerRotas.listarTodas()) { //todas as rotas de novo
                    if (r2.getOrigem() == aux && r2.getDestino() == dest) { //se a rota 2 sair do aux e chegar no destino
                        rotas.add(r1);
                        rotas.add(r2);
                    }
                }
            }
        }
        for (Rota r : rotas) {
            Tracado tr2 = new Tracado();
            tr2.setWidth(2);
            tr2.setCor(Color.BLUE);
            //Se for a rota principal, fica mais grossa e preta
            if (r.getOrigem() == orig && r.getDestino() == dest) {
                tr2.setWidth(4);
                tr2.setCor(Color.BLACK);
            }
            tr2.addPonto(r.getOrigem().getLocal());
            tr2.addPonto(r.getDestino().getLocal());
            if (r.getOrigem() == orig && r.getDestino() == dest) tr2.setWidth(3);
            //tr2.setLabel("Dist:" + distancia + "Aeronave:" + aviao);
            gerenciador.addTracado(tr2);
        }
        Tracado pto = new Tracado();
        pto.addPonto(orig.getLocal());
        List<MyWaypoint> lstPoints = new ArrayList<>();
        lstPoints.add(new MyWaypoint(Color.BLACK, orig.getNome() + " (" + orig.getCodigo() + ")", orig.getLocal(), 3));
        lstPoints.add(new MyWaypoint(Color.BLACK, dest.getNome() + " (" + dest.getCodigo() + ")", dest.getLocal(), 3));
        gerenciador.setPontos(lstPoints);
        gerenciador.getMapKit().repaint();
        zeraBotoes();
    }


    /**
     * CONSULTA 4
     * Selecionar um aeroporto de origem e mostrar todos os aeroportos que são alcançáveis
     * até um determinado tempo de vôo (ex: 12 horas), com no máximo duas conexões.
     */

    private void consulta4(int minRecebido) {
        gerenciador.clear();
        //RECONHECIMENTO DO AEROPORTO SELECIONADO
        if (gerenciador.getPosicao() == null) {
            //System.err.println("Nenhuma posicao selecionada.");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erro");
            alert.setHeaderText(null);
            alert.setContentText("Nenhuma Posição selecionada");
            alert.showAndWait();
            return;
        }

        Geo pos = new Geo(gerenciador.getPosicao().getLatitude(), gerenciador.getPosicao().getLongitude());
        Aeroporto origem = gerAero.toArray().get(0);
        double geo_distancia = Geo.distancia(pos, origem.getLocal());


        for (Aeroporto a : gerAero.toArray()) {
            Geo geo_aero = a.getLocal();
            double dist = Geo.distancia(pos, geo_aero);
            if (dist <= geo_distancia) {
                origem = a;
                geo_distancia = dist;
            }
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("SUCESSO!");
        alert.setHeaderText(null);
        alert.setContentText(origem.getNome() + " Selecionado!");
        alert.showAndWait();
        //System.out.println(pos.toString() + origem.getNome());
        Tracado tr2 = new Tracado();
        tr2.addPonto(origem.getLocal());
        List<MyWaypoint> lstPoints = new ArrayList<>();
        lstPoints.add(new MyWaypoint(Color.BLACK, origem.getNome() + " (" + origem.getCodigo() + ")", origem.getLocal(), 10));


        //LISTAS USADAS
        //List<MyWaypoint> lstPoints = new ArrayList<>();
        Set<Aeroporto> aeroportos = new HashSet<Aeroporto>(); //Recebe todos os aeroportos com max 2 escalas da origem e menos de 12h
        //ArrayList <Aeroporto> aeroEscalas = new ArrayList();
        Set<Aeroporto> aeroEscala1 = new HashSet<>(); //Recebe a 1 escala
        //Set<Aeroporto> aeroEscala2 = new HashSet<>(); //SE TIVER 3 CONEXOES


        //PERCORRE TODAS AS ROTAS E PROCURA AS ROTAS EM QUE O AEROPORTO SELECIONADO É ORIGEM
        // E ADICIONA O DESTINO DE SUA ROTA EM ESCALA1 E NA LISTA FINAL
        for (Rota r : gerRotas.listarTodas()) {
            double distancia = Geo.distancia(origem.getLocal(), r.getDestino().getLocal());
            double tempo = Math.round((distancia / 805.0) * 60.0);
            if (origem == r.getOrigem() && tempo <= minRecebido) {
                aeroportos.add(r.getDestino());
                aeroEscala1.add(r.getDestino());
            }
        }

        //PERCORRE A LISTA DE aeroEscalas1 E PARA CADA AEROPORTO DA LISTA, PERCORRe A LISTA DE ROTAS
        // E VERIFICA SE A ORIGEM DA ROTA É O AEROPORTO DA LISTA E SE ENTRE O AEROPORTO SELECIONADO(ORIGEM)
        // E O DESTINO DESSA ROTA SÃO 12 HORAS OU MENOS DE VOO, SE SIM, ADICIONA O DESTINO DA ROTA NA LISTA FINAL
        for (Aeroporto aeroporto : aeroEscala1) {
            for (Rota r : gerRotas.listarTodas()) {
                double distancia = Geo.distancia(origem.getLocal(), r.getDestino().getLocal());
                double tempo = Math.round((distancia / 805.0) * 60.0);
                if (r.getOrigem() == aeroporto && tempo <= (minRecebido - 30)) { // desconta 30 minutos por conta de 1 parada
                    aeroportos.add(r.getDestino());
                    //aeroEscala2.add(r.getDestino()); // se tiver 3 conexoes
                }
            }
        }
        // SE TIVER 3 CONEXOES
//        //PERCORRE A LISTA DE aeroEscalas2 E PARA CADA AEROPORTO DA LISTA, PERCORRe A LISTA DE ROTAS
//        // E VERIFICA SE A ORIGEM DA ROTA É O AEROPORTO DA LISTA E SE ENTRE O AEROPORTO SELECIONADO(ORIGEM)
//        // E O DESTINO DESSA ROTA SÃO 12 HORAS OU MENOS DE VOO, SE SIM, ADICIONA O DESTINO DA ROTA NA LISTA FINAL
//        for (Aeroporto aeroporto : aeroEscala2) {
//            for (Rota r : gerRotas.listarTodas()) {
//                double distancia = Geo.distancia(origem.getLocal(), r.getDestino().getLocal());
//                double tempo = Math.round((distancia / 805.0) * 60.0);
//                if (r.getOrigem() == aeroporto && tempo <= minRecebido - 60) { //660 desconta 1 hora por conta de 2 paradas
//                    aeroportos.add(r.getDestino());
//                }
//            }
//        }


        //PRINTA OS AEROPORTOS NO MAPA
        for (Aeroporto aero : aeroportos) {
            Tracado tr = new Tracado();
            tr.addPonto(aero.getLocal());
            lstPoints.add(new MyWaypoint(Color.RED, aero.getNome() + " (" + aero.getCodigo() + ")", aero.getLocal(), 6));
        }

        gerenciador.setPontos(lstPoints);
        gerenciador.getMapKit().repaint(); // Redesenha o mapa
    }


    private class EventosMouse extends MouseAdapter {
        private int lastButton = -1;

        @Override
        public void mousePressed(MouseEvent e) {
            JXMapViewer mapa = gerenciador.getMapKit().getMainMap();
            GeoPosition loc = mapa.convertPointToGeoPosition(e.getPoint());
            // System.out.println(loc.getLatitude()+", "+loc.getLongitude());
            lastButton = e.getButton();
            // Botão 3: seleciona localização
            if (lastButton == MouseEvent.BUTTON3) {
                gerenciador.setPosicao(loc);
                gerenciador.getMapKit().repaint();
            }
        }
    }

    private void createSwingContent(final SwingNode swingNode) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                swingNode.setContent(gerenciador.getMapKit());
            }
        });
    }
}




